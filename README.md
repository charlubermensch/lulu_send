<div align="center">

<img src="https://gitlab.com/uploads/-/system/project/avatar/33976567/o_lulu.jpg" width="200">

# LuluSend

[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://tldrlegal.com/license/gnu-general-public-license-v2)

Send a book (start a print-job) using the [Lulu](https://lulu.com) [API](https://developers.lulu.com/home).

[Getting started](#getting-started) •
[Installation](#installation) •
[Further](#further)

</div>

# Getting started

Write `~/.config/lulu` in the format:

```
client-key
client-private
book-cover
book-interior
Lulu SKU
project name
```

Client keys and client private are available at https://developers.lulu.com/user-profile/api-keys. If you're trying with the sandbox, you can have them at https://developers.sandbox.lulu.com/user-profile/api-keys. It requires another account to get access to this sandbox as well.

Book cover and book interior have to be **public https links** not path to files, they can be provided by one's own website or simply a drive.

Lulu SKU is given in this page: https://developers.lulu.com/price-calculator

Project name is simply a name. There isn't any use as of the author knowledge.

When calling the software, one has to have `address.json` in one's **working directory**.

```
{
        "city": "",
        "country_code": "",
        "name": "",
        "phone_number": "",
        "postcode": "",
        "state_code": "",
        "street1": ""
}
```

You can know run the software

```
python lulu.py <email>
```

<b>If you don't want to use the sandbox, change it in lulu.py itself by editing `IS_SANDBOX`.</b>

Email is an email that will receive "news" about the book.

At success it normally shows a JSON file.


# Installation

Go to the repo

```
git clone https://gitlab.com/charlubermensch/lulu_send.git
cd lulu_send
```

Install the dependency

```
pip install Authlib
```

You may do it in a virtual environment

```
virtualenv .venv
source .venv/bin/activate
```

# Further

<details>
<summary>Support</summary>

You may open an issue whether it's a bug or a documentation issue.
</details>

<details>
<summary>Further knowledge</summary>

What's based upon `make_authenticated_request()` function: https://api.lulu.com/docs/#section/Getting-Started/Make-authenticated-requests

Why using https://api.lulu.com/auth/realms/glasstree/protocol/openid-connect/token (API_TOKEN) when fetching token?

According to [the docs of Lulu](https://api.lulu.com/docs/#section/Authentication), it is the URL for the "clientCredentials OAuth Flow" Security Scheme Type. Auth0.com [doesn't discourage using it](https://auth0.com/docs/get-started/authentication-and-authorization-flow/client-credentials-flow) unlike the [other](https://auth0.com/docs/get-started/authentication-and-authorization-flow/implicit-flow-with-form-post) [ones](https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow).

You may use a sandbox environment. It requires to use the URL https://api.sandbox.lulu.com/ and first of all to make [another account](https://developers.sandbox.lulu.com/).
</details>

<details>
<summary>Roadmap</summary>

The config file may be moved from `~/.config` onto `/etc/` if it helps with servers.

The address file will either:
- be in `/tmp` with full path as parameter
- or be given entirely through `sys.argv`

## Roadmap

- [x] Write a private draft
- [x] Write a draft that split personal info in another file
- [x] Clean up the code base
- [x] Clean up again
- [x] Make run something
- [x] Make permanent and temporary parameters different
- [x] Add sandbox

The software isn't finished at all it's just that what will happen next isn't prepared yet. There may be some unittests.

</details>

<details>
<summary>Contributing</summary>

Run `pylama` in order to check for a good code quality.

Avoid adding features.

Avoid a coding style which isn't the one of the author. (no OO, no f-string, avoid editing the `pylama.ini`'s `ignore`)

The author has a webpage about his [coding style](https://charlubermensch.com/articles/programming/).

The project has been writen in Python in accordance to the skills of the author and in order to make a draft fast. However, there will be a transition to other languages, i.e. Go, C(++), Shell Script, Forth, ...
</details>

# Authors and acknowledgment

The only author is as of now [Charl'Übermensch](https://charlubermensch.com).

# License

This code is licensed under the GNU General Public License version 2 or later. (SPDX: GPL-2.0-or-later)

Reuse of the code is possible under GPLv3 (or later) but the author is willing to use the GPLv2 for the software itself.

This is free software: you are fre eto change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

The module which the software is based upon has for him a [BSD 3-Clause License](https://github.com/oauthlib/oauthlib/blob/master/LICENSE).
