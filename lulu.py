"""Ship Lulu."""

import sys

from os import environ

from json import loads

from subprocess import run as subrun  # no issue with own run()

from authlib.integrations.requests_client import OAuth2Session

import requests

IS_SANDBOX = False

SANDBOX_URL = ".sandbox" if IS_SANDBOX else ""

API_TOKEN = f"https://api{SANDBOX_URL}.lulu.com/auth/realms/glasstree/protocol/openid-connect/token"

URL = f"https://api{SANDBOX_URL}.lulu.com/print-jobs/"

RAW_PAYLOAD = """
{
    "contact_email": "%s",
    "external_id": "first-time",
    "line_items": [
        {
            "external_id": "item-reference-1",
            "printable_normalization": {
                "cover": {
                    "source_url": "%s"
                },
                "interior": {
                    "source_url": "%s"
                },
                "pod_package_id": "%s"
            },
            "quantity": 1,
            "title": "%s"
        }
    ],
    "production_delay": 120,
    "shipping_address": %s,
    "shipping_level": "MAIL"
}
"""


def get_config() -> list:
    """Return `~/.config/lulu` splitted by line."""
    with open("%s/.config/lulu" % environ["HOME"], "rb") as file:
        b_file = file.read()

    s_file = str(b_file, encoding="utf-8")

    return s_file.split("\n")


def get_address() -> str:
    """Return address."""
    with open("address.json", "rb") as file:
        b_file = file.read()
    return str(b_file, encoding="utf-8")


def get_payload() -> str:
    """Return payload."""
    address = get_address()

    config = get_config()

    email = sys.argv[1]
    cover = config[2]
    interior = config[3]
    package_id = config[4]
    title = config[5]

    return RAW_PAYLOAD % (
        email,
        cover,
        interior,
        package_id,
        title,
        address
    )


def assert_json_none(json: dict, key: str):
    """Write to `sys.stderr` a warning if `json`[`key`] isn't None."""
    if json[key] is not None:
        msg_warn = "warn: key %s is not of type None"
        print(msg_warn % key, file=sys.stderr)


def assert_json_warn(json: dict, key: str, typ: type):
    """Write to `sys.stderr` a warning if `json`[`key`] isn't of type `typ`."""
    if key not in json:
        msg_warn = "warn: key %s is not even in JSON"
        print(msg_warn % key, file=sys.stderr)
        return

    if typ is None:
        assert_json_none(json, key)
        return

    if not isinstance(json[key], typ):
        msg_warn = "warn: key %s is not of type %s"
        print(msg_warn % (key, str(typ)), file=sys.stderr)


def make_authenticated_request(token: str):
    """Make authenticated request(s)."""
    request = [
        "curl", "-X", "GET", URL,
        "-H", "Authorization: Bearer %s" % token,
        "-H", "Content-Type: application/json"
    ]

    process = subrun(request, capture_output=True)

    if process.returncode != 0:
        print("error: getting the authenticated request failed", file=sys.stderr)
        print(str(process.stderr, encoding="utf-8"), file=sys.stderr)
        sys.exit(1)

    output = str(process.stdout, encoding="utf-8")
    result = loads(output)

    assert_json_warn(result, "count", int)
    assert_json_warn(result, "next", None)
    assert_json_warn(result, "previous", None)
    assert_json_warn(result, "results", list)


def get_token(scope="") -> str:
    """Get token, make an authenticated request and return it."""
    config = get_config()
    client = OAuth2Session(config[0], config[1], scope=scope)
    response_json = client.fetch_token(API_TOKEN)
    token = response_json["access_token"]

    make_authenticated_request(token)

    return token


def run():
    """Send request of print-job and print response."""
    if len(sys.argv) == 1:
        print("error: no input email", file=sys.stderr)
        sys.exit(1)

    headers = {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Authorization': f'Bearer {get_token()}'
    }

    payload = get_payload()

    response = requests.request('POST', URL, data=payload, headers=headers)

    print(response.text)


if __name__ == "__main__":
    run()
