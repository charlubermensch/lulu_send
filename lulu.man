.TH SP 7 "13 Mars 2022"
.SH NAME
lulu \- Send a book (start a print-job) using the Lulu API.
.SH SYNOPSIS
.SY lulu
.OP email
.YS
.SH CONFIG/PREPARATION
.I ~/.config/lulu
has to be in this format:

.RS
client-key
.br
client-private
.br
book-cover
.br
book-interior
.br
Lulu SKU
.br
project name
.RE
.SH DESCRIPTION
.I address.json
has to be in working directory with this format:

.RS
{
.RS
"city": "",
.br
"country_code": "",
.br
"name": "",
.br
"phone_number": "",
.br
"postcode": "",
.br
"state_code": "",
.br
"street1": ""
.RE
}
.RE
.SH "REPORTING BUGS"
GitLab issues: <https://gitlab.com/charlubermensch/lulu_send/-/issues>
.SH BUGS
If there are errors with authentication, check the config file. (\fI~/.config/lulu\fP)
.SH "SEE ALSO"
python(1),https://developers.lulu.com/home
.SH AUTHOR
Written by "Charl'Ubermensch" (pseudonyme).
.LP
.UR https://gitlab.com/charlubermensch
Remote Git repositories
.UE
.LP
.MT inlife.developper@tutanota.com
CharlUbermensch
.ME
.br
.MT contact@charlubermensch.com
CharlUbermensch
.ME
.SH COPYRIGHT
License GPLv2: GNU General Public License version 2.
<https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>
.PP
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
